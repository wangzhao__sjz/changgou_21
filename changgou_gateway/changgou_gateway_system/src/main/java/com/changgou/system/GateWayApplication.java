package com.changgou.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


@SpringBootApplication
@EnableEurekaClient
public class GateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class);
    }
    //定义一个KeyResolver
    //根据ip限流
    @Bean
    public KeyResolver ipKeyResolver() {
        return new KeyResolver() {
            @Override
            public Mono<String> resolve(ServerWebExchange exchange) {
                return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
            }
        };
    }
}
/**
 * 总结: 在gateway网关的启动类下 配置一个KeyResolver ip限流 当然也可以用户限流 和 接口限流
 *          然后在yml配置文件下配置限流的过滤器的信息
 *          注意事项:
 *          filter名称必须是RequestRateLimiter 请求数据限流
 *          redis-rate-limiter.replenishRate：允许用户每秒处理多少个请求
 *          redis-rate-limiter.burstCapacity：令牌桶的容量，允许在一秒钟内完成的最大请求数
 *          key-resolver：使用SpEL按名称引用bean (用于限流的键的解析器的 Bean 对象的名字。
 *                          它使用 SpEL 表达式根据#{@beanName}从 Spring 容器中获取 Bean 对象)
 */