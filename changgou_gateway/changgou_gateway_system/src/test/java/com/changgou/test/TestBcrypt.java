package com.changgou.test;


import org.springframework.security.crypto.bcrypt.BCrypt;

public class TestBcrypt {
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            //获取盐
            String gensalt = BCrypt.gensalt();
            System.out.println("盐值:"+gensalt);
            //对密码加密
            String saltPassword = BCrypt.hashpw("666666", gensalt);
            System.out.println("加密后:"+ saltPassword);

            //解密
            boolean checkpw = BCrypt.checkpw("666666", saltPassword);
            System.out.println(checkpw);

        }

    }
}
