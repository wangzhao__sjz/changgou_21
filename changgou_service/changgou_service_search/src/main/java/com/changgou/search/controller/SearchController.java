package com.changgou.search.controller;


import com.changgou.entity.Page;
import com.changgou.search.pojo.SkuInfo;
import com.changgou.search.service.SearchService;
import org.omg.CORBA.portable.InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/search")
public class SearchController {
    //需要调用业务层接口
    @Autowired
    private SearchService searchService;

    @GetMapping("/list")
    //需要执行查询操作 所以需要条件
    public String list(@RequestParam Map<String, String> searchMap, Model model) {

   /*     if(null == searchMap.get("keywords")){
            searchMap.put("searchMap","");
        }*/

        //特殊符号的处理
        this.handleSearchMap(searchMap);
        //获取查询结果
        Map resultMap = searchService.search(searchMap);
        //通过model携带查询条件数据到页面
        model.addAttribute("searchMap", searchMap);
        //通过model携带查询结果数据到页面
        model.addAttribute("result", resultMap);

        //封装分页数据并返回
        //1.总记录数
        //2.当前页
        //3.每页显示条数
        Page<SkuInfo> page = new Page<SkuInfo>(
                Long.parseLong(String.valueOf(resultMap.get("total"))),
                Integer.parseInt(String.valueOf(resultMap.get("pageNum"))),
                Page.pageSize
        );
        model.addAttribute("page",page);

        //拼装URL
        //1.创建一个stringbuilder
        StringBuilder url = new StringBuilder("/search/list");
        if (searchMap != null && searchMap.size() > 0) {
            //有查询条件
            url.append("?");
            for (String paramKey : searchMap.keySet()) {
                //不是分页规则 排序条件 页码才追加条件
                if (!"sortRule".equals(paramKey) && !"sortFiled".equals(paramKey) && !"pageNum".equals(paramKey)) {
                    url.append(paramKey).append("=").append(searchMap.get(paramKey)).append("&");
                }
            }
            //urlString 拼接的是地址栏的字符串
            String urlString = url.toString();
            //去除最后一个&
            urlString = urlString.substring(0, urlString.length() - 1);
            //通过model添加url
            model.addAttribute("url", urlString);
        } else {
            model.addAttribute("url", url);
        }

        return "search";
    }

    @GetMapping
    @ResponseBody
    public Map search(@RequestParam Map<String, String> searchMap) {

        //get请求携带的参数拼接在路径上 会被编码解码 所以需要解决特殊符号的问题
        this.handleSearchMap(searchMap);
        Map searchResult = searchService.search(searchMap);
        return searchResult;
    }

    //get请求携带的参数拼接在路径上 会被编码解码 所以需要解决特殊符号的问题
    private void handleSearchMap(Map<String, String> searchMap) {
        Set<Map.Entry<String, String>> entries = searchMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            if (entry.getKey().startsWith("spec_")) {
                searchMap.put(entry.getKey(), entry.getValue().replace("+", "%2B"));

            }
        }
    }


}
